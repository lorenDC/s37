const jwt = require('jsonwebtoken');
// User defined string data that will be used to create our JSON web tokens
// Used in the algorithm for encrypting our data which makes it difficult to decode the information without the defined secret keyword

const secret = "CourseBookingAPI";

/*
- JSON Web Token or JWT is a way of securely passing information from the server to the frontend or to other parts of server
- Information is kept secure through the use of the secret code
- Only the system that knows the secret code that can decode the encrypted information
*/


module.exports.createAccessToken = (user) => {
	// The data will be received from the registration for,
	// When the user logs in, a token will be create with user's information
	const data = {
		id: user._id,
		email: user.email,
		isAdmin: user.isAdmin
	};

	// Generate a JSON web Token using the jwt.sign() method
	// Generates the token using the form data and secret code with no additional options provided.
	return jwt.sign(data, secret, {}) //(<payload>,secretkey, {})
};


module.exports.verify = (request, response, next) => {

	let token = request.headers.authorization
	console.log(token)

	if(typeof token !== "undefined") {
		token = token.slice(7, token.length)
		return jwt.verify(token, secret, (error, data) => {
			if(error){
				return response.send({auth: "failed"})
			} else {
				next()
			}
		})
	} else {
		return response.send({auth: "failed"})
	}
};


module.exports.decode = (token) => {
	if(typeof token !== "undefined") {
		token = token.slice(7, token.length)
		return jwt.verify(token, secret, (error, data) => {
			if(error){
				return null
			} else {
				return jwt.decode(token, {complete: true}.payload)
			}
		})
	} else {
		return null
	}
};