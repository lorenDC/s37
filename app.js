const express = require('express')
const mongoose = require('mongoose')
const cors = require('cors')
const userRoutes = require('./routes/userRoutes')
const courseRoutes = require('./routes/courseRoutes')

const app = express();
const port = process.env.PORT || 4000;

// Middlewares
app.use(cors());
app.use(express.json());
app.use(express.urlencoded({extended: true}))

mongoose.connect(`mongodb://lrndlacrz:admin123@ac-mvlgpdf-shard-00-00.c2nhgts.mongodb.net:27017,ac-mvlgpdf-shard-00-01.c2nhgts.mongodb.net:27017,ac-mvlgpdf-shard-00-02.c2nhgts.mongodb.net:27017/S37-S41?ssl=true&replicaSet=atlas-ika0ge-shard-0&authSource=admin&retryWrites=true&w=majority`, {
	useNewUrlParser: true,
	useUnifiedTopology: true
});

// Main URI
app.use("/users", userRoutes)
app.use("/courses", courseRoutes)


let db = mongoose.connection;

db.on('error', console.error.bind(console, 'Connection'));
db.once('open', () => console.log('Connectd to server!'));

app.listen(port, () => console.log(`API is running at ${port}`))
