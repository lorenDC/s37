const express = require('express')
const router = express.Router()
const courseController = require('../controllers/courseController')
const auth = require('../auth')

// Route for creating a course
router.post("/", auth.verify, (request, response) => {
	const userData =  auth.decode(request.headers.authorization)
	console.log(userData)
	courseController.addCourse(request.body, {id: userData.id, isAdmin: userData.isAdmin}).then(resultFromController => response.send(resultFromController))
});


router.get("/all", auth.verify, (request, response) => {
	const userData =  auth.decode(request.headers.authorization)
	console.log(userData)
	courseController.getAllCourses(userData).then(resultFromController => response.send(resultFromController))
});

router.get("/", (request, response) => {
	courseController.getAllActive().then(resultFromController => response.send(resultFromController))
});

router.get("/:courseId", (request, response) => {
	console.log(request.params.courseId)
	courseController.getCourse(request.params).then(resultFromController	=> response.send(resultFromController))
})

router.put("/:courseId", auth.verify, (request, response) => {
	const isAdminData = auth.decode(request.headers.authorization).isAdmin
	console.log(isAdminData)
	console.log(request.params.courseId)
	courseController.updateCourse(request.body, request.params, isAdminData).then(resultFromController => response.send(resultFromController))
})

router.put("/:courseId/archive", auth.verify, (request, response) => {
	const isAdminData = auth.decode(request.headers.authorization).isAdmin
	courseController.archiveCourse(request.body, request.params, isAdminData).then(resultFromController => response.send(resultFromController))
})


module.exports = router