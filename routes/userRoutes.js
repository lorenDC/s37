const express = require('express')
const router = express.Router()
const userController = require('../controllers/userController')
const auth = require('../auth')

// Routes

// checkEmail
router.post("/checkEmail", (request, response) => {
	userController.checkEmailExists(request.body).then(resultFromController => response.send(resultFromController))
});

router.post("/register", (request, response) => {
	userController.registerUser(request.body).then(resultFromController => response.send(resultFromController))
});

router.post("/login", (request, response) => {
	userController.loginUser(request.body).then(resultFromController => response.send(resultFromController))
});

router.get("/details", auth.verify, (request, response) => {
	const userData =  auth.decode(request.headers.authorization)
	console.log(userData)
	userController.getProfile({id: userData.id}).then(resultFromController => response.send(resultFromController))
});

router.post("/enroll", auth.verify, (request, response) => {
	const checkAdmin = auth.decode(request.headers.authorization).isAdmin
	const getId = auth.decode(request.headers.authorization).id

	let data = {
		userId: getId,
		courseId: request.body.courseId
	}

	userController.enroll(data, checkAdmin).then(resultFromController => response.send(resultFromController))
})

module.exports = router;