const User = require('../models/User')
const Course = require('../models/Course')
const bcrypt = require('bcrypt')
const auth = require('../auth')

module.exports.checkEmailExists = (request => {
	return User.find({email: request.email}).then(result => {
		if(result.length > 0) {
			return true
		} else {
			return false
		}
	})
});

// registration
module.exports.registerUser = (request => {
	let newUser = new User({
		firstName: request.firstName,
		lastName: request.lastName,
		email: request.email,
		password: bcrypt.hashSync(request.password, 10),
		mobileNo: request.mobileNo
	})
	return newUser.save().then((userResult, error) => {
		if(error){
			return false
		} else {
			return userResult
		}
	})
});

// loginUser
module.exports.loginUser = (request) => {
	return User.findOne({email: request.email}).then(result => {
		if(result == null) {
			return false
		} else {
			// compareSync(<dataToCompare>, <encryopted password>)
			const isPasswordCorrect = bcrypt.compareSync(request.password, result.password)
			if(isPasswordCorrect) {
				return {access: auth.createAccessToken(result)}
			} else {
				return false
			}
		}
	})
};

// User details
module.exports.getProfile = (userData) => {
	return User.findById(userData.id).then((result, error) => {
		console.log(userData)
		if(error) {
			return false
		} else {
			result.password = "***"
			return result
		}
	})
};

module.exports.enroll = async (data, checkAdmin) => {
	 if (checkAdmin == false){
		 	let isUserUpdated = await User.findById(data.userId).then(user => {user.enrollments.push({courseId: data.courseId})
		 	return user.save().then((user, error) => {
		 		if(error){
		 			return false
		 		} else {
		 			return true
		 		}
		 	})
		});

		 let isCourseUpdated = await Course.findById(data.courseId).then(course => {
		 	course.enrollees.push({userId: data.userId})

		 	return course.save().then((course, error) => {
		 		if(error){
		 			return false
		 		} else {
		 			return true
		 		}
		 	})
		 });

		 if (isUserUpdated && isCourseUpdated) {
		 	return true
		 } else {
		 	return false
		 };
	 } else {
	 	return "You are an admin!"
	 }
};