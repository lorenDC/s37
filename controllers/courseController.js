const Course = require('../models/Course')
const User = require('../models/User')
const auth = require('../auth')

// Create a new course
module.exports.addCourse = ((request, userData) => {
	return User.findById(userData.id).then((result, error) => {
		if(userData.isAdmin == false ) {
			return "Not an admin!"
		} else {
			let newCourse = new Course ({
				name: request.name,
				description: request.description,
				price: request.price
			});

			return newCourse.save().then((course, error) => {
				if(error){
					return false
				} else {
					return course
				}
			})
		}
	}) 
});

module.exports.getAllCourses = (data => {
	if(data.isAdmin) {
		return Course.find({}).then(result => {
			return result
		})
	} else {
		return false
	}
});

module.exports.getAllActive = () => {
	return Course.find({isActive: true}).then(result => {
		return result
	})
};

module.exports.getCourse = (request) => {
	return Course.findById(request.courseId).then(result => {
		return result
	})
};

module.exports.updateCourse = (request, reqParams, data) => {
	if(data){
		let updatedCourse = {
			name: request.name,
			description: request.description,
			price: request.price
		}
		return Course.findByIdAndUpdate(reqParams.courseId, updatedCourse).then((updatedCourse, error) => {
			if(error){
				return false
			} else {
				return updatedCourse
			}
		})
	} else {
		return "Not an admin!"
	}
};

module.exports.archiveCourse = (request, reqParams, data) => {
	if(data){
		let updatedStatus = {
			isActive: request.isActive
		}
		console.log(reqParams.courseId)
		return Course.findByIdAndUpdate(reqParams.courseId, updatedStatus).then((updatedStatus, error) => {
			if(error){
				return false
			} else {
				return true
			}
		})
	} else {
		return "Not an admin!"
	}
}